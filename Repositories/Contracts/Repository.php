<?php

namespace Afaqy\OrderModule\Repositories\Contracts;

use Afaqy\Core\Repositories\Contracts\BaseRepository;

interface OrderModuleRepository extends BaseRepository
{
}
