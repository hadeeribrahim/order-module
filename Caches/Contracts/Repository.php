<?php

namespace Afaqy\OrderModule\Caches\Contracts;

use Afaqy\Core\Caches\Contracts\BaseCache;

interface OrderModuleCache extends BaseCache
{
}
